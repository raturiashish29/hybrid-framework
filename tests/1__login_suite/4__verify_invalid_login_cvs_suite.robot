*** Settings ***
Documentation   this suite verify Invalid users are not able to login to dashboard
...     and connected to testcase TC_OH_03 and also
...     has test data via excel


Resource    ../../resource/base/CommonFunctionalities.resource

Library     DataDriver      file=../../test_data/verify_invalid_login.csv
Test Setup      Launch Browser And Navigate To Url
Test Teardown   Close Browser
Test Template   Verify Invalid Login Template

*** Test Cases ***
Verify Invalid Login ${testcase_number}


*** Keywords ***
Verify Invalid Login Template
        [Arguments]     ${username}     ${password}     ${expected_error}
        Input Text    name=username    ${username}
        Input Text    name=password    ${password}
        Click Element    xpath=//button[contains(normalize-space(),'Login')]
        Element Text Should Be    xpath=//p[contains(normalize-space(),'Invalid credentials')]    ${expected_error}