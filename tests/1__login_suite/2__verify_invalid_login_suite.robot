*** Settings ***
Documentation   This suite file verifies invalid users are not allowed to login to the dashboard
...     and it is connected to test case TC_OH_03
...     Test Template Concepts

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown   Close Browser

Test Template   Verify Invalid Login Template

*** Test Cases ***
TC1
    saul     saul123     Invalid credentials
TC2
    ${EMPTY}      kim123      Required
TC3
    Jessy      ${EMPTY}      Required


*** Keywords ***
Verify Invalid Login Template
    [Arguments]     ${username}     ${password}     ${expected_error}
    Input Text    name=username    ${username}
    Input Text    name=password    ${password}
    Click Element    xpath=//button[normalize-space()='Login']
    IF    '${username}'=='${EMPTY}'
        Element Should Contain    xpath=//input[@name='username']/../../span    ${expected_error}
    ELSE IF     '${password}'=='${EMPTY}'
        Element Should Contain    xpath=//input[@name='password']/../../span    ${expected_error}
    ELSE
        Element Should Contain    xpath=//p[contains(normalize-space(),'Invalid')]        ${expected_error}
    END
