*** Settings ***
Documentation   This suite file verifies valid users are able to login to the dashboard
...     and connected to test case TC_OH_02

Resource    ../../resource/base/CommonFunctionalities.resource

Library     DataDriver      file=../../test_data/orange_data_employee.xlsx   sheet_name=EmployeeDetails
Library    OperatingSystem
Test Setup      Launch Browser And Navigate To Url
Test Teardown   Close Browser
Test Template   Verify Add Valid Employee Template

*** Test Cases ***
TC1

*** Keywords ***
Verify Add Valid Employee Template
    [Arguments]     ${username}     ${password}  ${firstname}    ${middlename}   ${lastname}
    Input Text    name=username    ${username}
    Input Text    name=password    ${password}
    Click Element    xpath=//button[normalize-space()='Login']
    Click Element   xpath=//span[normalize-space()='PIM']
    Click Element   link=Add Employee
    Input Text    name=firstName   ${firstname}
    Input Text    name=middleName   ${middlename}
    Input Text    name=lastName   ${lastname}
    Choose File    xpath=//input[@type='file']    C:${/}Automation Concepts${/}HybridFramework${/}test_data${/}image.jpg
    Click Element   xpath=//button[contains(normalize-space(),'Save')]
    sleep   7s
    Element Should Contain    xpath=//h6[contains(normalize-space(),'${firstname} ${lastname}')]    ${firstname} ${lastname}
    ${fn}   Get Value    name=firstName
    Should Be Equal    ${fn}     ${firstname}

#    Element Text Should Be    name=firstName    ${firstname}
    
